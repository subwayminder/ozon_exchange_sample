<?php

namespace Ozon;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Monolog\Logger;

class OzonExchange
{

    /**
     * @var Client
     */
    private Client $guzzleClient;
    /**
     * @var Client
     */
    private Client $b24Client;
    /**
     * @var Logger
     */
    private Logger $log;

    /**
     * OzonExchange constructor.
     */
    public function __construct()
    {

        $this->guzzleClient = new Client([
            'base_uri' => Constants::URL,
            'timeout' => 5.0,
            'headers' => [
                'Client-Id' => Constants::CLIENT_ID,
                'Api-Key' => Constants::TOKEN,
            ]
        ]);

        $this->b24Client = new Client([
            'base_uri' => Constants::WEBHOOK,
        ]);
        $this->log = new Logger('request_log');
    }

    /**
     * @param string $method
     * @param array $query
     * @return mixed
     * @throws GuzzleException
     */
    public function sendRequestToOzon(string $method, array $query)
    {
        try {
            $response = $this->guzzleClient->post($method, $query);
            return json_decode($response->getBody(), 1)['result'];
        }
        catch (GuzzleException $exception){
            sleep(10);
            return $this->sendRequestToOzon($method, $query);
        }
    }

    /**
     * @param string $status
     * @param int $limit
     * @param int $offset
     * @return array[]
     */
    private function getOzonDeals(string $status, int $limit, int $offset) :array
    {
        return [
            'json' => [
                "dir" => "asc",
                "filter" => [
                    "status" => $status
                ],
                "limit" => $limit,
                "offset" => $offset,
                "with" => [
                    "barcodes" => true
                ]
            ]
        ];
    }

    /**
     * Основной алгоритм обмена сделок между Ozon и Б24
     * @param int $limit
     * @param string $status
     * @throws GuzzleException
     */
    public function goodsExchange(int $limit, string $status): void
    {
        $step = $limit <= 50 ? $limit : 50;

        for ($offset = 0; $offset <= $limit; $offset+=$step){

            $ozonResponse = $this->sendRequestToOzon('v2/posting/fbs/list', $this->getOzonDeals($status, $limit, $offset));

            /**
             * Поиск существующих сделок
             */
            $queryList = [];
            foreach ($ozonResponse as $deal) {
                $queryListRow = [$deal['order_id'] =>
                    'crm.deal.list?' . $this->makeB24Query('crm.deal.list', $deal)];
                $queryList += $queryListRow;
            }

            $crmDealListResponse = $this->b24RequestWrapper('batch', $this->buildBatch($queryList));

            /**
             * Добавление сделок
             */
            $queryList = [];
            $existsDeals = [];
            foreach ($ozonResponse as $deal) {
                if ($this->checkIfDealExist($deal,$crmDealListResponse) ) {
                    $queryListRow = [$deal['order_id'] =>
                        'crm.deal.add?' . $this->makeB24Query('crm.deal.add', $deal)];
                    $queryList += $queryListRow;
                }
                if (isset($crmDealListResponse[$deal['order_id']][0]['ID'])){
                    $existsDeals += [$deal['order_id'] => $crmDealListResponse[$deal['order_id']][0]['ID']];
                }
            }

            $crmDealAddResponse = $this->b24RequestWrapper('batch', $this->buildBatch($queryList));

            /**
             * Обновление сделок
             */
            $queryList = [];
            foreach ($ozonResponse as $deal) {
                if (isset($existsDeals[$deal['order_id']])) {
                    $queryListRow = [$deal['order_id'] =>
                        'crm.deal.update?' . $this->makeB24Query('crm.deal.update', $deal, $existsDeals[$deal['order_id']])];
                    $queryList += $queryListRow;
                }
            }
            $this->b24RequestWrapper('batch', $this->buildBatch($queryList));

            /**
             * Поиск существующих товаров
             */
            $queryList = [];
            foreach ($ozonResponse as $deal) {
                foreach ($deal['products'] as $product) {
                    $queryListRow = [$product['offer_id'] =>
                        'crm.product.list?' . $this->makeB24Query('crm.deal.update',$product)];
                    $queryList += $queryListRow;
                }
            }
            $crmProductListResponse = $this->b24RequestWrapper('batch', $this->buildBatch($queryList));

            /**
             * Добавление товаров в Б24
             */
            $queryList = [];
            $existsProducts = [];
            foreach ($ozonResponse as $deal) {
                foreach ($deal['products'] as $product) {
                    if ( !( ($crmProductListResponse[$product['offer_id']][0]['ID']) > 0) ) {
                        $queryListRow = [$product['offer_id'] => 'crm.product.add?' . $this->makeB24Query('crm.product.add',$product)];
                        $queryList = array_merge($queryList, $queryListRow);
                    }
                    if (($crmProductListResponse[$product['offer_id']][0]['ID'])>0){
                        $existsProducts += [$product['offer_id'] => $crmProductListResponse[$product['offer_id']][0]['ID']];
                    }
                }
            }
            $crmProductAddResponse = $this->b24RequestWrapper('batch', $this->buildBatch($queryList));

            /**
             * Добавление товаров в сделки Б24
             */
            $productsAr = $crmProductAddResponse + $existsProducts;
            $queryProductRowSet = [];
            if (!empty($crmDealAddResponse)) {
                foreach ($ozonResponse as $deal) {
                    if (!empty($crmDealAddResponse[$deal['order_id']]))
                    $b24DealID = $crmDealAddResponse[$deal['order_id']];
                    $queryList = [];
                    foreach ($deal['products'] as $product) {
                        if (isset($productsAr[$product['offer_id']])) {
                            $queryList += [
                                'PRODUCT_ID' => $productsAr[$product['offer_id']],
                                'PRICE' => (int)$product['price'],
                                'QUANTITY' => $product['quantity']
                            ];
                        }
                    }
                    $queryProductRowSet += [$b24DealID => 'crm.deal.productrows.set?' . $this->CrmDealProductrowsSet($queryList, $b24DealID)];
                }
                $this->b24RequestWrapper('batch', $this->buildBatch($queryProductRowSet));
            }
        }
    }

    /**
     * Обертка для запроса в B24. Служит для отлавливания лимита по запросам
     * @param string $method
     * @param array $query
     * @return array
     */
    private function b24RequestWrapper(string $method, array $query){
        /**
         * @var array $response
         */
        try {
            $response = json_decode($this->b24Client->post($method. '?auth='.Constants::TOKEN, ['json' => $query])->getBody(), true)['result']['result'];
        }
        catch (GuzzleException $exception){
            $this->log->error($exception->getMessage());
            $this->log->info(Constants::TOKEN);
            $error = $exception->getMessage();
            if(stripos($error,'QUERY_LIMIT_EXCEEDED')!== false){
                sleep(15);
                $response = $this->b24RequestWrapper($method, $query);
            }else{
                $response = $this->b24RequestWrapper($method, $query);
            }

        }
        return $response;
    }

    /**
     * Обобщающая функция для создания запросов в B24
     * @param string $method
     * @param array $item
     * @param int $b24Id
     * @return string
     * @throws GuzzleException
     */
    public function makeB24Query(string $method, array $item, int $b24Id=0): string
    {
        $query = 'Такого метода не существует';
        switch ($method) {
            case 'crm.deal.add':
                $query = $this->CrmDealAdd($item);
                break;
            case 'crm.deal.list':
                $query = $this->CrmDealList($item);
                break;
            case 'crm.product.list':
                $query = $this->CrmProductList($item);
                break;
            case 'crm.product.add':
                $query = $this->CrmProductAdd($item);
                break;
            case 'crm.deal.update':
                $query = $this->CrmDealUpdate($item, $b24Id);
                break;
        }
        return $query;
    }

    /**
     * Возвращает запрос для добавления сделки в B24
     * @param array $item
     * @return string
     */
    private function CrmDealAdd(array $item): string
    {
        return http_build_query(array(
            'fields' => [
                'TITLE' => 'OZON ' . $item['posting_number'],
                'STAGE_ID' => 'NEW',
                'STAGE_SEMANTIC_ID' => 'P',
                'ASSIGNED_BY_ID' => 1,
            ]
        ));
    }

    /**
     * Возвращает запрос для листинга сделки в B24
     * @param array $item
     * @return string
     */
    private function CrmDealList(array $item): string
    {
        return http_build_query(array(
            "filter" => [
                'TITLE' => 'OZON ' . $item['posting_number']
            ],
            "select" => [
                "ID"
            ]
        ));
    }

    /**
     * @param array $product
     * @return string
     */
    private function CrmProductList(array $product) :string
    {
        return http_build_query(array(
            "filter" => [
                Constants::PRODUCT_FOREIGN_KEY => $product['offer_id']
            ],
            "select" => [
                "ID"
            ]
        ));
    }

    /**
     * Добавляет товар в b24
     * @param array $product
     * @return string
     */
    private function CrmProductAdd(array $product): string
    {
        return http_build_query([
            'fields' => [
                'NAME' => $product['name'],
                'CURRENCY_ID' => 'RUB',
                'PRICE' => $product['price'],
                Constants::PRODUCT_FOREIGN_KEY => $product['offer_id']
            ]
        ]);
    }

    /**
     * Возвращает запрос для добавления товаров в Б24
     * @param array $rows
     * @param int $dealID
     * @return string
     */
    private function CrmDealProductrowsSet(array $rows, int $dealID): string
    {
        return http_build_query(
            [
                'id' => $dealID,
                'rows' => [$rows]
            ]
        );
    }

    /**
     * Возвращает запрос для обновления сделки в Б24
     * @param array $item
     * @param int $b24Id
     * @return string
     * @throws GuzzleException
     */
    private function CrmDealUpdate (array $item, int $b24Id) :string {
        $category = preg_replace("/[^0-9]/", '', stristr(Constants::AR_OZON_STATUSES[$item['status']], ':', true));
        $queryArray = [
            'id' => $b24Id,
            'fields' => [
                'STAGE_ID' => Constants::AR_OZON_STATUSES[$item['status']],
                'CATEGORY_ID' => $category?$category:1,
                'STAGE_SEMANTIC_ID' => 'P',
                'UF_CRM_OZON_STATUS' => $item['status']
            ]
        ];
        $packageLabel = $this->getPackageLabel($item['posting_number']);
        if($packageLabel){
                $queryArray['fields']['UF_CRM_OZON_BARCODE'] = [
                    'fileData' => [
                        'OZON'.$item['posting_number'].'.pdf',
                        base64_encode($packageLabel)
                    ]
                ];
        }
        return http_build_query($queryArray);
    }

    /**
     * Получение штрихкода в заказе
     * @param string $postingNumber
     * @return bool|string
     * @throws GuzzleException
     */
    protected function getPackageLabel(string $postingNumber)
    {
        $query = [
            'posting_number' => [
                $postingNumber
            ]
        ];
        $result = $this->sendRequestToOzon('v2/posting/fbs/package-label', $query);
        if(is_array(json_decode($result,1)) || json_decode($result,1)['error']['code'] == 'POSTINGS_NOT_READY') return false;
        return $result;
    }

    protected function checkIfDealExist (array $deal, array $crmDealListResponse) :bool
    {
        return $deal['order_id']>0  &&  !($crmDealListResponse[$deal['order_id']][0]['ID'] > 0);
    }

    /**
     * Сборка запроса для батча в B24
     * @param array $queryList
     * @param int $halt
     * @return array
     */
    private function buildBatch(array $queryList, int $halt = 0): array
    {
        return [
            'halt' => $halt,
            'cmd' => $queryList
        ];
    }
}
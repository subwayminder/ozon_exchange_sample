<?php


namespace Ozon;


class Constants
{
    const URL = 'https://api-seller.ozon.ru/';
    const TOKEN = 'a401c834-3b5f-4a65-a12e-4b37f831ab40';
    const CLIENT_ID = '114539';
    const WEBHOOK = 'https://b24-45v2tx.bitrix24.ru/rest/1/08arj7bi0s7q0r3f/';
    const DEBUG = true;
    const PRODUCT_FOREIGN_KEY = 'PROPERTY_107';
    const AR_OZON_STATUSES = [
        "awaiting_packaging" => 'OZON_AWAITING_CONFIRMATION',
        "delivering" => 'OZON_DELIVERING_STATUS',
        "delivered" => 'OZON_DELIVERED',
        "cancelled" => 'OZON_CANCELED',
        "awaiting_deliver" => 'OZON_AWAITING_SHIPMENT',
        "driver_pickup " => 'AT_THE_DRIVER',
        "arbitration" => 'PENDING_DECISION',
        "not_accepted" => 'CONTROVERSIAL'
    ];
}